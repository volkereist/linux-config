# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Set your current project here, which should be located in ~/projects/
export CURRENT_PROJECT="autodoc"

# Path to your oh-my-zsh installation.
if [ -d /home/palm07 ]; then
    export ZSH=/home/palm07/.oh-my-zsh
elif [ -d /home/Martin ]; then
    export ZSH=/home/Martin/.oh-my-zsh
else
    export ZSH=~/.oh-my-zsh
fi


# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"
# ZSH_THEME="wezm"
# ZSH_THEME="bullet-train"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
 HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"
 ENABLE_CORRECTION="false"

# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
 HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# plugins=(zsh-256color, git, autopep8, celery, colorize, debian, ng, django, github, heroku, jsontools, postgres, shrink-path, tmux, ubuntu)
plugins=(zsh-256color, git, colorize, jsontools, svn)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

#lazy stuff
alias cd..="cd .."
alias ..="cd .."
alias ...="cd ../.."
alias l='ls -CF'
alias ll='ls -AlsF'
alias la='ls -AFC'
alias sz='source ~/.zshrc'

# projects
if [ -d /home/palm07 ]; then
    PROJECTS_ROOT="/cygdrive/d/Dev/git"
else
    PROJECTS_ROOT="~/projects"
fi
alias project="cd $PROJECTS_ROOT/$CURRENT_PROJECT"
alias gov="cd $VIRTUAL_ENV/bin"
alias journal="cd $PROJECTS_ROOT/goals"

# git stuff
alias startover='git status | grep "modified" | awk "{print \$2}" | xargs -I{} git checkout -- {}'

# tmux stuff
alias tmux="TERM=screen-256color-bce tmux"
alias tm="tmux new-session"
alias tl="tmux list-sessions"
alias ta="tmux attach -t"

# django stuff
alias rabbit="invoke-rc.d rabbitmq-server"
alias dtest="project && python manage.py test"
alias celeryworker="celery -A $CURRENT_PROJECT worker -l info"
alias runserver="cd $PROJECTS_ROOT/$CURRENT_PROJECT && python manage.py runserver"
alias collect="cd $PROJECTS_ROOT/$CURRENT_PROJECT && python manage.py collectstatic"

# edit linux-config
alias linux-config="cd ~/linux-config && git status"
alias editzshrc="vim ~/linux-config/.zshrc"
alias editvimrc="vim ~/linux-config/.vimrc"
alias edittmux="vim ~/linux-config/tmux.config"

# heroku stuff
alias hl="heroku login"

# vps stuff
alias finnegan="ssh root@$FINNEGAN_VPS"

# source virtualenvwrapper.sh
export PROJECT_HOME=/cygdrive/d/Daten/dev/workspace
export GOPATH=$HOME/Go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

# svn prompt stuff
prompt_svn() {
    local rev branch
    if in_svn; then
        rev=$(svn_get_rev_nr)
        branch=$(svn_get_branch_name)
        if [[ $(svn_dirty_choose_pwd 1 0) -eq 1 ]]; then
            prompt_segment yellow black
            echo -n "$rev@$branch"
            echo -n "±"
        else
            prompt_segment green black
            echo -n "$rev@$branch"
        fi
    fi
}

# Export editor for SVN
export SVN_EDITOR=vim

# load local aliases
if [ -f ~/.zsh_aliases ]; then
    . ~/.zsh_aliases
fi


